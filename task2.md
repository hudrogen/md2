https://docs.oracle.com/javase/specs/jls/se7/html/jls-17.html
https://habrahabr.ru/post/133981/ 
https://docs.spring.io/spring/docs/4.3.17.BUILD-SNAPSHOT/spring-framework-reference/htmlsingle/
  
  7.4. Dependencies
  
  7.5. Bean scopes
  
  7.9. Annotation-based container configuration

* Практическая часть (результатом выполнения заданий являются unit-тесты):
  1. Дано: файл в формате key(строка)-value(объект, можно рассматривать как строку)
     Реализовать потокобезопасный сервис с кешом значений, считанных из файла, и обновлением при заданном количестве обращений. 
     Доказать предсказуемость набора значений в кеше.
    
  2. Реализовать не блокирующий потокобезопасный BigInteger sequence (см. java.util.concurrent.atomic.AtomicReference)
    interface Sequence {
      BigInteger next();
      BigInteger curval();
    }
    Доказать потокобезопасность данного sequence'а.

  3. Показать все возможные варианты разрешения конфликтов в spring framework

  4. Показать разницу между prototype и singleton скоупом бинов
    Показать, что бины не являются потокобезопасными.
