package ru.innopolis.task4;

public interface Bean {
    Integer getField();
    void setField(Integer field);
}
