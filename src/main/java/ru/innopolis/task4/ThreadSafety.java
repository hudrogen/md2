package ru.innopolis.task4;

import org.springframework.stereotype.Component;

@Component
public class ThreadSafety {

    private Integer counter = 0;

    private int incValue = 0;

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public int incAndGet(){
        return ++incValue;
    }

}
