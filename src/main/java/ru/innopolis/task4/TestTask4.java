package ru.innopolis.task4;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class TestTask4 {

    @Autowired
    @Qualifier("bean1")
    private Bean beanImpl11;

    @Autowired
    @Qualifier("bean1")
    private Bean beanImpl12;

    @Autowired
    @Qualifier("bean2")
    private Bean beanImpl21;

    @Autowired
    @Qualifier("bean2")
    private Bean beanImpl22;

    @Autowired
    private ThreadSafety threadSafety;

    @Test
    public void testSingleton(){
        beanImpl11.setField(15);
        beanImpl12.setField(17);

        assertEquals("17", beanImpl11.getField().toString());
        assertNotEquals("15", beanImpl11.getField().toString());
    }

    @Test
    public void testPrototype(){
        beanImpl21.setField(15);
        beanImpl22.setField(17);

        assertEquals("15", beanImpl21.getField().toString());
        assertNotEquals("17", beanImpl21.getField().toString());
    }


    @Test
    /**Непотокобезхопасность бинов спринга*/
    public void test2(){
        Thread t1 = new Thread(() -> {
            for (int i = 1; i < 100000; i++) {
                int j = threadSafety.incAndGet();
                System.out.println("1 поток = " +i + " and " + j);
                if (i != j){
                    assertTrue(false);
                }
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 100000; i++) {
                int j = threadSafety.incAndGet();
                System.out.println("2 поток = " + i + " and " +j);
                if (i != j){
                    assertTrue(false);
                }
            }
        });
        t1.start();
        t2.start();
    }

}
