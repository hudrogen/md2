package ru.innopolis.task4;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("bean1")
@Scope("singleton")
public class BeanImpl1 implements Bean{

    private Integer field;

    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }
}
